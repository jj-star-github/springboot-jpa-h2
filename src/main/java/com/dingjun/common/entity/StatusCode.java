package com.dingjun.common.entity;

/**
 *  返回的状态码
 * @author 22372
 */
public class StatusCode {

    public static final int OK=20000;//成功
    public static final int ERROR=20001;// 失败

}
