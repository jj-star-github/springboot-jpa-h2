package com.dingjun.mapper;

import com.dingjun.pojo.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
public interface H2Mapper extends JpaRepository<User, Integer> {
}
