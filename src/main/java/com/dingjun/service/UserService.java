package com.dingjun.service;

import com.alibaba.fastjson.JSON;
import com.dingjun.mapper.H2Mapper;
import com.dingjun.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@Service
@Slf4j
public class UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private H2Mapper h2Mapper;

    /**
     * 直接向H2数据库里面存一个user数据
     *
     * @param user
     */
    public void saveUser(User user) {
        LOGGER.info("into method saveUser,入参：" + JSON.toJSONString(user));
        user.setId((int) Math.random());
        h2Mapper.save(user);
    }


    /**
     * 查询数据库表里的所有用户数据
     *
     * @return
     */
    public List<User> findAll() {
        LOGGER.info("into method findAll：");
        return h2Mapper.findAll();
    }


    /**
     * 批量增加user
     *
     * @param users
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean batchSaveUser(List<User> users) {
        LOGGER.info("into method batchSaveUser,入参：" + JSON.toJSONString(users));
        if (null != users) {
            h2Mapper.saveAll(users);
        }
        return true;
    }


    /**
     * 修改单个用户用户信息
     *
     * @param user
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public User updateUser(User user) {
        LOGGER.info("into method updateUser,入参：" + JSON.toJSONString(user));
        List<User> userList = this.findAll();
        if (!StringUtils.isEmpty(userList)) {
            for (User userDB : userList) {
                if (null != user && user.getId().equals(userDB.getId())) {
                    h2Mapper.save(user);
                }
            }
        }
        return user;
    }


    /**
     * 批量修改用户用户信息
     * //todo 此功能有问题
     * @param userList
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public List<User> batchUpadteUsers(List<User> userList) {
        LOGGER.info("into method batchUpadteUsers,入参：" + JSON.toJSONString(userList));
        List<User> users = this.findAll();
        if (StringUtils.isEmpty(users)) {
            log.info("修改的用户不能为空！");
        } else {
            for (User user : userList) {
                if (null != user) {
                    for (User userDB : users) {
                        if (user != userDB) {
                            h2Mapper.saveAll(userList);
                        }
                    }
                }
            }
        }
        return userList;
    }

    /**
     * 根据任意id批量查询
     *
     * @param idList
     * @return
     */
    public List<User> batchFindUserById(List<Integer> idList) {
        LOGGER.info("into method batchFindUserById,入参：" + JSON.toJSONString(idList));
        return h2Mapper.findAllById(idList);
    }
}
