package com.dingjun.controller;

import com.dingjun.common.entity.Result;
import com.dingjun.common.entity.StatusCode;
import com.dingjun.pojo.User;
import com.dingjun.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 22372
 * @Description:
 * @Version: V1.0
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 直接向H2数据库里面存一个user数据
     *
     * @param user
     * @return
     */
    @PostMapping("/saveUser")
    public Result saveUser(@RequestBody User user) {
        userService.saveUser(user);
        return new Result(true, StatusCode.OK, "添加成功！");
    }


    /**
     * 批量增加user
     *
     * @param users
     * @return
     */
    @PostMapping("/batchSaveUsers")
    public Result batchSaveUsers(@RequestBody List<User> users) {
        return new Result(true, StatusCode.OK, "批量添加成功！", userService.batchSaveUser(users));
    }

    /**
     * 查询数据库表里的所有用户数据
     *
     * @return
     */
    @GetMapping("/userFindAll")
    public Result userFindAll() {
        List<User> users = userService.findAll();
        return new Result(true, StatusCode.OK, "查询全部用户成功！", users);
    }


    /**
     * 根据任意id批量查询
     *
     * @param idList
     * @return
     */
    @GetMapping("/batchFindUserById/{idList}")
    public Result batchFindUserById(@PathVariable List<Integer> idList) {
        return new Result(true, StatusCode.OK, "查询全部用户成功！", userService.batchFindUserById(idList));
    }

    /**
     * 修改单个用户信息
     *
     * @param user
     * @return
     */
    @PutMapping("/updateUser")
    public Result updateUser(@RequestBody User user) {
        return new Result(true, StatusCode.OK, "修改单个用户成功！", userService.updateUser(user));
    }


    /**批量修改用户用户信息
     *
     * @param userList
     * @return
     */
    @PutMapping("/batchUpadteUsers")
    public Result batchUpadteUsers(List<User> userList) {
        return new Result(true, StatusCode.OK, "批量修改用户成功！", userService.batchUpadteUsers(userList));
    }
}
