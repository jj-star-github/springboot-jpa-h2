package user;

import com.dingjun.UserApplication;
import com.dingjun.pojo.User;
import com.dingjun.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @Description:
 * @Version: V1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = UserApplication.class)
public class H2Test {

    @Autowired
    private UserService userService;


    /**
     * 添加一个新的用户信息
     */
    @Test
    public void addUserTest() {
        User user = new User();
        user.setName("丁军军");
        user.setPhone("134566334");
        userService.saveUser(user);
    }

    /**
     * 查询数据库表里的所有用户数据
     */
    @Test
    public void userFindAll() {
        System.out.println(userService.findAll());
    }
}
